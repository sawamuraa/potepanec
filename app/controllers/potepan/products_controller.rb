class Potepan::ProductsController < ApplicationController
  LIMIT_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.includes(master: [:images, :default_price]).
      related_products(@product).limit(LIMIT_OF_RELATED_PRODUCTS)
  end
end
