require "rails_helper"

RSpec.feature "Products", type: :feature do
  describe "display product details" do
    let(:taxonomy) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, name: "Clothing", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:shirts) { create(:taxon, name: "Shirts", taxonomy: taxonomy, parent: taxon) }
    let(:product) { create(:product, price: "23.45", taxons: [taxon]) }
    let(:related_products) do
      5.times.map do
        create(:product, price: "#{rand(1.0..99.9).round(2)}", taxons: [taxon])
      end
    end

    let(:thumbnails) { page.find("#carousel").all("img") }
    let(:related_product_thumbnails) { page.all(".productImage img") }

    before do
      image = File.open(
        Rails.root.join("spec", "fixtures", "test.png")
      )
      product.images.create!(attachment: image)
      product.images.create!(attachment: image)

      related_products.each do |related_product|
        related_product.images.create!(attachment: image)
      end

      visit potepan_product_path(product.id)
    end

    scenario "display each product details" do
      expect(page).to have_link 'home', href: potepan_path
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
      expect(page).to have_title "#{product.name} - Potepanec"
      expect(page).to have_selector ".page-title h2", text: product.name
      expect(page).to have_selector ".col-xs-6 li", text: product.name
      expect(page).to have_selector ".media-body h2", text: product.name
      expect(page).to have_selector "h3", text: product.display_price
      expect(page).to have_content product.description
      expect(thumbnails.size).to eq product.images.size
      expect(thumbnails[0]["src"]).to eq product.images[0].attachment(:large)
      expect(thumbnails[1]["src"]).to eq product.images[1].attachment(:large)
      expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
    end

    scenario "goes to home with click_on header_logo" do
      within(".navbar-header") { find(".navbar-brand").click }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #nav_bar_to_home" do
      within(".navbar") { click_link "home" }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to home with click_on #top_bar_to_home" do
      within(".pageHeader") { click_link "Home" }
      expect(current_path).to eq potepan_path
    end

    scenario "goes to show with click_on relatedProduct" do
      related_products.first(4).each do |related_product|
        visit potepan_product_path(product.id)
        click_on related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end

    scenario "should be able to visit related products" do
      visit potepan_product_path(product.id)
      expect(page).not_to have_selector ".productCaption h5", text: product.name
      expect(page).not_to have_selector ".productCaption h3", text: product.display_price
      4.times.all? do |i|
        related_product = related_products[i]
        expect(page).to have_selector ".productCaption h5", text: related_product.name
        expect(page).to have_selector ".productCaption h3", text: related_product.display_price
        expect(related_product_thumbnails[i]["src"]).
          to eq related_product.images.first.attachment(:small)
      end
      expect(page).not_to have_selector ".productCaption h5", text: related_products.last.name
      expect(page).
        not_to have_selector ".productCaption h3", text: related_products.last.display_price
      expect(page).
        not_to have_selector("img[src$='#{related_products.last.images.first.attachment(:small)}']")
    end
  end
end
