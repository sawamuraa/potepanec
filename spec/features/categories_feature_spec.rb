require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "Clothing", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:bag) { create(:taxon, name: "bag", taxonomy: taxonomy, parent: taxonomy.root) }
  let(:shirts) { create(:taxon, name: "Shirts", taxonomy: taxonomy, parent: taxon) }
  let!(:product) do
    create(:product, name: "Ruby on Rails T-Shirt", price: "23.45", taxons: [shirts])
  end
  let!(:other_product) do
    create(:product, name: "other-bag", price: "98.76", taxons: [bag])
  end

  before do
    visit potepan_category_path(taxonomy.root.id)
  end

  scenario "View category show page" do
    expect(current_path).to eq potepan_category_path(taxonomy.root.id)
    expect(page).to have_title "#{taxonomy.root.name} - Potepanec"
    expect(page).to have_selector ".page-title h2", text: "Category"
    expect(page).to have_selector ".col-xs-6 .breadcrumb", text: "Home shop Category"
    expect(page).to have_selector ".panel-heading", text: "商品カテゴリー"
    expect(page).to have_selector ".panel-heading", text: "色から探す"
    expect(page).to have_selector ".panel-heading", text: "サイズから探す"
    expect(page).to have_selector ".panel-body", text: bag.name
    expect(page).to have_selector ".panel-body", text: shirts.name
    expect(page).to have_selector ".productCaption h5", text: product.name
    expect(page).to have_selector ".productCaption h3", text: product.display_price
    expect(page).to have_link "Home", href: potepan_path
  end
  scenario "Products for each category are displayed" do
    visit potepan_category_path(bag.id)
    expect(current_path).to eq potepan_category_path(bag.id)
    expect(page).to have_title "#{bag.name} - Potepanec"
    expect(page).not_to have_selector ".page-title h2", text: "clothing"
    expect(page).to have_selector ".page-title h2", text: "bag"
    expect(page).not_to have_selector ".productCaption h5", text: product.name
    expect(page).to have_selector ".productCaption h5", text: other_product.name
    expect(page).not_to have_selector ".productCaption h3", text: product.display_price
    expect(page).to have_selector ".productCaption h3", text: other_product.display_price
  end
  scenario "should be able to visit other category" do
    expect(page).to have_link bag.name, href: potepan_category_path(bag.id)
    expect(page).to have_link shirts.name, href: potepan_category_path(shirts.id)
  end
  scenario "should be able to visit products" do
    expect(page).to have_link product.name, href: potepan_product_path(product.id)
  end

  scenario "goes to home with click_on header_logo" do
    within(".navbar-header") { find(".navbar-brand").click }
    expect(current_path).to eq potepan_path
  end

  scenario "goes to home with click_on #nav_bar_to_home" do
    within(".navbar") { click_link "home" }
    expect(current_path).to eq potepan_path
  end

  scenario "goes to home with click_on #top_bar_to_home" do
    within(".pageHeader") { click_link "Home" }
    expect(current_path).to eq potepan_path
  end

  scenario "goes to specific category page with click_on category in side nav" do
    click_link shirts.name

    expect(page).to     have_current_path(potepan_category_path(shirts.id))
    expect(page).to     have_selector '.page-title', text: shirts.name
    expect(page).to     have_content product.name
    expect(page).to     have_content product.display_price
    expect(page).not_to have_content other_product.name
    expect(page).not_to have_content other_product.display_price
  end
end
