require 'rails_helper'

RSpec.describe "Product_model", type: :model do
  describe "related_products" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:bag_taxon) { create(:taxon, name: "Bag Taxon", taxonomy: category, parent: category.root) }
    let(:shirt_taxon) do
      create(:taxon, name: "Shirts Taxon", taxonomy: category, parent: category.root)
    end
    let(:product) { create(:product, taxons: [bag_taxon, shirt_taxon], name: "Product") }

    let(:related_products_scope) do
      Spree::Product.related_products(product)
    end

    let(:other_taxon) do
      create(:taxon, name: "Other_taxon", taxonomy: category, parent: category.root)
    end
    let!(:other_product) do
      create(:product, name: "other_product", price: "98.76", taxons: [other_taxon])
    end

    context "There are a related products" do
      let!(:related_products) { create_list(:product, 2, taxons: [bag_taxon, shirt_taxon]) }

      it "returns 2 distincted products" do
        expect(related_products_scope).to match_array related_products
        expect(related_products_scope.count).to eq 2
      end

      it "does not include target product" do
        expect(related_products_scope).not_to include product
      end

      it "does not include other taxon's product" do
        expect(related_products_scope).not_to include other_product
      end
    end
  end
end
