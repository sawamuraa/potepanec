require "rails_helper"
require "spree/testing_support/factories"

RSpec.describe "Potepan::Products page", type: :request do
  describe "potepan/products" do
    let(:taxonomy) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, name: "Clothing", taxonomy: taxonomy, parent: taxonomy.root) }
    let(:shirts) { create(:taxon, name: "Shirts", taxonomy: taxonomy, parent: taxon) }
    let(:product) { create(:product, taxons: [shirts]) }

    let(:other_taxon) do
      create(:taxon, name: "Other_taxon", taxonomy: taxonomy, parent: taxonomy.root)
    end
    let!(:related_products) { create_list(:product, 4, taxons: [shirts]) }
    let(:other_product) do
      create(:product, name: "other_product", price: "98.76", taxons: [other_taxon])
    end

    before do
      get potepan_product_path(product.id)
    end

    it "requests successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "show product name display" do
      expect(response.body).to include(product.name)
    end

    it "show template products display" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
      expect(response.body).to include related_products[0].name
      expect(response.body).to include related_products[0].display_price.to_s
      expect(response.body).to include related_products[1].name
      expect(response.body).to include related_products[1].display_price.to_s
      expect(response.body).not_to include other_product.name
      expect(response.body).not_to include other_product.display_price.to_s
    end

    it "assigns instance variables" do
      expect(assigns(:product)).to eq product
    end

    it "assigns related_products" do
      expect(assigns(:related_products)).to match_array related_products
      expect(assigns(:related_products)).not_to include product
      expect(assigns(:related_products)).not_to include other_product
    end
  end
end
